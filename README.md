# MineClearer2

![screenshot](https://gitlab.com/jsheradin/MineClearer2/raw/javafx/gameplay.PNG)

### About
This is a basic minesweeper game implemented in Java.

### Additional Info
Works with JDK 8. JavaFX is used for graphics.
